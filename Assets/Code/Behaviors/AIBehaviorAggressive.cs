﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AIBehaviorAggressive : MonoBehaviour {

    public Character myEnemy;
    public Character mCharacter; // Not fan of doing this but I think its easier now.
  

	public void Init(List<Character> PossibleEnemies, Character me, UnityEngine.UI.Text text)
    {
        if (myEnemy == null)
        {
            FindEnemy(PossibleEnemies);
        }
        mCharacter = me;
        StartCoroutine(ThinkingProcess(text));
        
    }

    public IEnumerator ThinkingProcess(UnityEngine.UI.Text text)
    {
        yield return new WaitForSeconds(0.3f);
        int enemyDistance = GridBasedPathfinding.DistanceTo(myEnemy.myPos, mCharacter.myPos);
        while (mCharacter.mAction == false || mCharacter.mMoved == false)
        {
            if (enemyDistance <= mCharacter.mWeapon.Range)
            {
                text.text = "Enemy Turning - Attacking";
                Action();
                while(mCharacter.mAction == false)
                {
                    Debug.Log("Bob");
                    yield return new WaitForSeconds(0.5f);
                }                             
            }            
            else
            {
                text.text = "Enemy Turning - Moving";
                MoveToLocation(GetCloserPositionAlternative());
                while (mCharacter.mMoved == false)
                {
                    yield return new WaitForSeconds(0.5f);
                }
                enemyDistance = GridBasedPathfinding.DistanceTo(myEnemy.myPos, mCharacter.myPos);
                if (enemyDistance > mCharacter.mWeapon.Range)
                {
                    mCharacter.mAction = true;
                    Debug.Log("222");
                }
            }
        }
        Debug.Log(mCharacter.mAction + "    " + mCharacter.mMoved);
        yield return new WaitForSeconds(0.3f);
        EndTurn();
    }
    
    public void FindEnemy(List<Character> PossibleEnemies)
    {

        foreach (var c in PossibleEnemies)
        {
            if (c.mSide == 0)
            {
                myEnemy = c;
                break;
            }
        }
    }

   void MoveToLocation(IntVector2 newPos)
    {
        var watch = System.Diagnostics.Stopwatch.StartNew();
        GridBasedPathfinding.instance.selectedCharacter = mCharacter;
        GridBasedPathfinding.instance.FindShortestRouteInitial(mCharacter, newPos, mCharacter.Move, mCharacter.Height);
        GridBasedPathfinding.instance.CallMoveCharacter(EndMove);
        watch.Stop();
        Debug.Log(watch.ElapsedMilliseconds + " ms");
    }

    public void EndMove()
    {
        mCharacter.mMoved = true;
    }

    public IntVector2 GetCloserPositionAlternative()
    {        
        
        GridBasedPathfinding.instance.CalculateMovementZone(mCharacter);

        IntVector2 correctedPos = CheckEmptySide();
        IntVector2 desiredPos = new IntVector2(0, 0);
        int chosen = 500;
        foreach (var v in GridBasedPathfinding.instance.highLightedArea)
        {
            if (MapCreator.myMap[v.x, v.y].passable)
            {               
                int distance = GridBasedPathfinding.DistanceTo(correctedPos, v);
                if (distance < chosen && chosen >= mCharacter.mWeapon.Range)
                {
                    chosen = distance;
                    desiredPos = v;
                }
            }
        } 
        return desiredPos;        
    }  

    public void Action() {        
      
            CombatManager.AttackBriefing myAttackInfo = GameObject.Find("GameManager").GetComponent<CombatManager>().ProjectAttackAI(mCharacter, myEnemy);
            GameObject.Find("GameManager").GetComponent<TurnManager>().currentBriefing = myAttackInfo;
            GameObject.Find("GameManager").GetComponent<TurnManager>().InitiateAttack();
            MapCreator.myMap[mCharacter.myPos.x, mCharacter.myPos.y].passable = false;          
        
    }

    public void EndTurn() {

        Debug.Log("EndTurn");
        //Temporary - Switch to a callback
        GameObject.Find("GameManager").GetComponent<TurnManager>().EndTurn();

    }

    IntVector2 CheckEmptySide()
    {
        IntVector2 correctedPos = new IntVector2(0, 0);
        byte exceptions = 0;
        if(MapCreator.myMap[myEnemy.myPos.x + 1, myEnemy.myPos.y].passable == false)
        {
            exceptions++;
        }
        else
        {
            correctedPos = new IntVector2(myEnemy.myPos.x + 1, myEnemy.myPos.y);
        }
        if (MapCreator.myMap[myEnemy.myPos.x - 1, myEnemy.myPos.y].passable == false)
        {
            exceptions++;
        }
        else
        {
            correctedPos = new IntVector2(myEnemy.myPos.x - 1, myEnemy.myPos.y);
        }
        if (MapCreator.myMap[myEnemy.myPos.x , myEnemy.myPos.y + 1].passable == false)
        {
            exceptions++;
        }
        else
        {
            correctedPos = new IntVector2(myEnemy.myPos.x, myEnemy.myPos.y + 1);
        }
        if (MapCreator.myMap[myEnemy.myPos.x, myEnemy.myPos.y - 1].passable == false)
        {
            exceptions++;
        }
        else
        {
            correctedPos = new IntVector2(myEnemy.myPos.x, myEnemy.myPos.y - 1);
        }

        if(exceptions != 3)
        {
            correctedPos = myEnemy.myPos;
        }
        return correctedPos;
    }
}
