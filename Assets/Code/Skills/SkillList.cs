﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillList : ScriptableObject {

    public static SkillList instance;
      
    public List<Skill> Skills = new List<Skill>();

}
