﻿
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Skill
{

    public string name;
    public string description;

    public Enums.skillType type;

    public int minDamage;
    public int maxDamage;
    public int range;
    public int areaOfEffect;    
    public int heightDifference;

    public float accuracyModifier;

    public bool trueHit;
public Skill(){

}
public virtual void CreateSkill() {
     
        
}
public virtual CombatManager.AttackBriefing BriefingSkill(Character attacker, Character defender, Tile t = null)
{
        CombatManager.AttackBriefing briefing = new CombatManager.AttackBriefing();
        briefing.attacker = attacker;
        if(defender != null){
        briefing.defender = defender;
        briefing.hitChance = attacker.mStatus.Accuracy + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge;
        briefing.minDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 0.25f);
        briefing.maxDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 1.25f);
        briefing.critChance = (attacker.mStatus.Luck * 3) + (attacker.mStatus.Accuracy * 2) - (defender.mStatus.Luck * 3);
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        }
        briefing.skill = true;
        return briefing;  
}

public virtual List<CombatManager.DamageReport> UseSkill(LineRenderer myTrail, CombatManager.AttackBriefing b) {
     
    return new List<CombatManager.DamageReport>();
}

public void endSkill(List<CombatManager.DamageReport> list, int dmg, CombatManager.AttackBriefing b){
    
    if(b.defender.mStatus.Health <= 0)
        {
            
            list.Add(new CombatManager.DamageReport(true, dmg, b.defender));
        }
        else
        {
           list.Add(new CombatManager.DamageReport(false, dmg, b.defender));
        } 
}
}
