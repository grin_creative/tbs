﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Skill
{
    public override void CreateSkill()
    {
        name = "Heavy Attack";
        description = "strong attack to but with less accuracy";
        type = Enums.skillType.SingleDamage;
        minDamage = 40;
        maxDamage = 50;
        range = 1;
        areaOfEffect = 1;
        heightDifference = 2;
        accuracyModifier = -5;
    }

    public Bomb()
    {
        name = "Bomb";
        description = "strong attack to but with less accuracy";
        type = Enums.skillType.CircleDamage;
        minDamage = 5;
        maxDamage = 10;
        range = 4;
        areaOfEffect = 3;
        heightDifference = 2;
        accuracyModifier = -5;
    }
    public override CombatManager.AttackBriefing BriefingSkill(Character attacker, Character defender, Tile t = null)
    {
        CombatManager.AttackBriefing briefing = new CombatManager.AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        briefing.hitChance = 90f;
        briefing.minDamage = 40;
        briefing.maxDamage = 50;
        briefing.critChance = 0;
        briefing.critMultiplier = 0;
        briefing.enemyHealth = defender.mStatus.Health;
        GridBasedPathfinding.instance.CleanSkill();
        GridBasedPathfinding.instance.SkillRange(name, attacker);
        
        int x = t.myPos.x;
        int y = t.myPos.y;
        t.highlight(Color.red);
        MapCreator.myMap[x - 1, y].highlight(Color.red);
        MapCreator.myMap[x + 1, y].highlight(Color.red);
        MapCreator.myMap[x, y - 1].highlight(Color.red);
        MapCreator.myMap[x, y + 1].highlight(Color.red);
        GridBasedPathfinding.instance.AttackArea.Add(t);

        GridBasedPathfinding.instance.AttackArea.Add(MapCreator.myMap[x - 1, y]);
        GridBasedPathfinding.instance.AttackArea.Add(MapCreator.myMap[x + 1, y]);
        GridBasedPathfinding.instance.AttackArea.Add(MapCreator.myMap[x, y - 1]);
        GridBasedPathfinding.instance.AttackArea.Add(MapCreator.myMap[x, y + 1]);

        return briefing;
    }
    public override List<CombatManager.DamageReport> UseSkill(LineRenderer myTrail, CombatManager.AttackBriefing b)
    {
        myTrail.positionCount = 0;
        var list = new List<CombatManager.DamageReport>();

        foreach (Character c in GameObject.Find("GameManager").GetComponent<TurnManager>().allCharacters)
        {
            foreach (Tile tile in GridBasedPathfinding.instance.AttackArea)
            {
                if (tile.myPos.x == c.myPos.x && tile.myPos.y == c.myPos.y)
                {
                    Debug.Log("triggered");
                    int dmg = 0;
                    if (UnityEngine.Random.Range(1, 100) <= 90)
                    {
                        int value = 1;
                        dmg = value * UnityEngine.Random.Range(minDamage, maxDamage);
                        c.mStatus.Health -= dmg;

                    }
                    else
                    {
                        dmg = -1;
                    }
                    Character temp = b.defender;
                    b.defender = c;
                    endSkill(list, dmg, b);
                    b.defender = temp;
                    break;
                }
            }
        }
        GridBasedPathfinding.instance.CleanSkill();
        return list;
    }

    public List<IntVector2> getPositions(IntVector2 mPos)
    {
        var possiblePos = new List<IntVector2>();
        if (areaOfEffect == 3)
        {
            possiblePos.Add(mPos);
            possiblePos.Add(new IntVector2(mPos.x + 1, mPos.y));
            possiblePos.Add(new IntVector2(mPos.x - 1, mPos.y));
            possiblePos.Add(new IntVector2(mPos.x, mPos.y + 1));
            possiblePos.Add(new IntVector2(mPos.x, mPos.y - 1));
            possiblePos.Add(new IntVector2(mPos.x + 1, mPos.y + 1));
            possiblePos.Add(new IntVector2(mPos.x - 1, mPos.y - 1));
            possiblePos.Add(new IntVector2(mPos.x - 1, mPos.y + 1));
            possiblePos.Add(new IntVector2(mPos.x + 1, mPos.y - 1));
        }
        return possiblePos;
    }
}
