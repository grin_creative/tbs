﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowRock : Skill
{
    public override void CreateSkill() {
		name = "Heavy Attack";
		description = "strong attack to but with less accuracy";
		type = Enums.skillType.SingleDamage;
		minDamage = 1;
		maxDamage = 5;
		range = 5;
		areaOfEffect = 1;
		heightDifference = 2;
		accuracyModifier = 0;
	}

    public ThrowRock() {
		name = "Throw Rock";
		description = "strong attack to but with less accuracy";
		type = Enums.skillType.SingleDamage;
		minDamage = 1;
		maxDamage = 5;
		range = 5;
		areaOfEffect = 1;
		heightDifference = 2;
		accuracyModifier = 0;
	}
	public override CombatManager.AttackBriefing BriefingSkill(Character attacker, Character defender ,Tile t = null){
		CombatManager.AttackBriefing briefing = new CombatManager.AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        briefing.hitChance = attacker.mStatus.Accuracy + accuracyModifier + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge;
        briefing.minDamage = 1 + minDamage;
        briefing.maxDamage = 1 + maxDamage;
        briefing.critChance = (attacker.mStatus.Luck * 3) + (attacker.mStatus.Accuracy * 2) - (defender.mStatus.Luck * 3);
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        return briefing;
	}
	public override List<CombatManager.DamageReport> UseSkill(LineRenderer myTrail, CombatManager.AttackBriefing b)
	{
		myTrail.positionCount = 0;
        var list = new List<CombatManager.DamageReport>();
        int dmg = 0;
        if (UnityEngine.Random.Range(1,100) <= b.hitChance + accuracyModifier)
        {
            int value = 1;
            if (UnityEngine.Random.Range(1, 100) <= b.critChance + accuracyModifier)
            {
                value = (int)b.critMultiplier;
            }
            dmg = value * UnityEngine.Random.Range(1+minDamage, 1+maxDamage);
            b.defender.mStatus.Health -= dmg;
        }
        else
        {
            dmg = -1;          
        }
        endSkill(list,dmg,b);
		return list;
	}
}
