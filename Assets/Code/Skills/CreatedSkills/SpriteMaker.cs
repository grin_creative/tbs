﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteMaker : MonoBehaviour
{

    public List<GameObject> mSprites = new List<GameObject>();

	//Value to break row
	int mLineSize = 3;
	//Value of the space between sprites at the X space
	int xOffset = 4;
	//Value of the space between sprites at the Y space
	int yOffset = -8;
	//Put the value of the texture width here
	int mTextureWidth = 512;
	//Put the value of the texture height here
	int mTextureHeight = 1024;

    // Use this for initialization
    void Start()
    {

        CreateSprites(new int[5] { 0, 1, 0, 1, 0 });
    }
    void CreateSprites(int[] array)
    {
        int counter = 0;
        int columm = 0;
		int row = 0;
        foreach (int i in array)
        {
			mSprites.Add(AddSprite(Resources.Load("Texture" + i) as Texture2D, "name"+counter, mTextureWidth, mTextureHeight));
            mSprites[counter].transform.position = new Vector3(xOffset * row, yOffset * columm, 0);
            counter++;
			row++;
			if (row - mLineSize == 0)
            {				
                columm++;
				row = 0;
            }            
        }
    }
    public GameObject AddSprite(Texture2D tex, string name, int width, int height)
    {
        Texture2D _texture = tex;
        Sprite newSprite = Sprite.Create(_texture, new Rect(0f, 0f, width, height), new Vector2(0.5f, 0.5f), 128f);
        GameObject sprGameObj = new GameObject();
        sprGameObj.name = name;
        sprGameObj.AddComponent<SpriteRenderer>();
        SpriteRenderer sprRenderer = sprGameObj.GetComponent<SpriteRenderer>();
        sprRenderer.sprite = newSprite;
        return sprGameObj;
    }

}
