﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongCut : Skill
{
    public override void CreateSkill() {
		name = "Heavy Attack";
		description = "strong attack to but with less accuracy";
		type = Enums.skillType.SingleDamage;
		minDamage = 3;
		maxDamage = 3;
		range = 2;
		areaOfEffect = 1;
		heightDifference = 2;
		accuracyModifier = 5;
	}

    public LongCut(){
		name = "Long Cut";
		description = "strong attack to but with less accuracy";
		type = Enums.skillType.SingleDamage;
		minDamage = 3;
		maxDamage = 3;
		range = 2;
		areaOfEffect = 1;
		heightDifference = 2;
		accuracyModifier = -5;
	}
	public override CombatManager.AttackBriefing BriefingSkill(Character attacker, Character defender ,Tile t = null){
		CombatManager.AttackBriefing briefing = new CombatManager.AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        briefing.hitChance = attacker.mStatus.Accuracy + accuracyModifier + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge;
        briefing.minDamage = attacker.mWeapon.Damage+minDamage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 0.25f);
        briefing.maxDamage = attacker.mWeapon.Damage+maxDamage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 1.25f);
        briefing.critChance = (attacker.mStatus.Luck * 3) + (attacker.mStatus.Accuracy * 2) - (defender.mStatus.Luck * 3);
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        return briefing;
	}
	public override List<CombatManager.DamageReport> UseSkill(LineRenderer myTrail, CombatManager.AttackBriefing b)
	{
		myTrail.positionCount = 0;
        var list = new List<CombatManager.DamageReport>();
        int dmg = 0;
        if (UnityEngine.Random.Range(1,100) <= b.hitChance + accuracyModifier)
        {
            int value = 1;
            if (UnityEngine.Random.Range(1, 100) <= b.critChance + accuracyModifier)
            {
                value = (int)b.critMultiplier;
            }
            dmg = value * UnityEngine.Random.Range(b.minDamage+minDamage, b.maxDamage+maxDamage);
            b.defender.mStatus.Health -= dmg;

        }
        else
        {
            dmg = -1;          
        }
        endSkill(list,dmg,b);
		return list;
    }
}
