﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour {
	public int mSize;
	private Vector3 _boxPosition = Vector3.zero;
	public IntVector2 myPos;
	Transform HighBox;
	MeshRenderer HighBoxMesh;
	public bool highlighted;
	public bool passable = true;
    public bool confirmMovement = false;

	void Start(){
		
	}

	public void AddTileBox(Transform t){
		Transform box = Instantiate(t);
		box.parent = transform;
		box.localPosition = _boxPosition;
		if(HighBox == null){
			HighBox = transform.Find("Highl");
			HighBoxMesh = HighBox.GetComponent<MeshRenderer>();
			HighBoxMesh.enabled = false;
			
		}
		HighBox.localPosition = new Vector3(_boxPosition.x,_boxPosition.y+0.26f,_boxPosition.z);				
		_boxPosition.y += 0.5f;
		mSize ++;
		highlighted = false;
	}		

	public void PaintBox(Color color, int i = -1){
		if(i == -1){
			i = transform.childCount-1;
		}
		transform.GetChild(i).GetComponent<MeshRenderer>().material.color = color;
	}

	public void ElevateGround(){
		_boxPosition.y += 0.5f;
	}

	public void RemoveGroundTile(){
		Destroy(transform.GetChild(1).gameObject);
	}

	public void highlight(){	
		if(highlighted == false){
			//PaintBox(new Color32(150,150,150,255));
			HighBoxMesh.enabled = true;
			highlighted = true;
		}			
	}

    public void highlight(Color32 color, bool confirm = false)
    {       
        HighBoxMesh.enabled = true;
        transform.GetChild(0).GetComponent<MeshRenderer>().material.color = color;
        highlighted = true;
        confirmMovement = confirm;
    }

    public void Clear(){
		highlighted = false;
		HighBoxMesh.enabled = false;
	}
}
