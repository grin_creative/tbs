﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class Character : MonoBehaviour
{
    public int mTurn = 0;
    public int Move = 5;
    public int Jump = 3;
    public int Height = 1;
    public bool mMoved = false;
    public bool mAction = false;
    public int mSide = 0; // 0 = Player, 1 = Enemy, 2 = Ally(AI) 
    public IntVector2 myPos = new IntVector2(0, 0);
    public IntVector2 desiredPos = new IntVector2(0, 0);
    public List<IntVector2> highLightedArea = new List<IntVector2>();
    public Status mStatus;
    public Weapon mWeapon;
    public List<Skill> mSkills;
   
    void Start()
    {
        //Temporary Testing Values        
        SetMainStatus();
        SetSecondaryStatus();
        SetSkills();
        
    }

    private void SetMainStatus()
    {
        mStatus = new Status();       
        mStatus.Agility = UnityEngine.Random.Range(1, 20);
        mStatus.Move = 3 + Mathf.Clamp((int)(mStatus.Agility * 0.3f), 2, 5);
        Move = mStatus.Move;
        mStatus.maxHealth = UnityEngine.Random.Range(30, 60);
        mStatus.Health = mStatus.maxHealth;
        mStatus.maxFocus = UnityEngine.Random.Range(30, 60);
        mStatus.Focus = mStatus.maxFocus;
        mStatus.Strength = UnityEngine.Random.Range(3, 8);
        mStatus.Stamina = UnityEngine.Random.Range(3, 8);
        mStatus.Defense = UnityEngine.Random.Range(3, 8);
        mStatus.Luck = UnityEngine.Random.Range(3, 8);
        mStatus.baseStamina = mStatus.Stamina;
        mStatus.baseStrength = mStatus.Strength;
        mStatus.baseDefense = mStatus.Defense;
        mStatus.baseLuck = mStatus.Luck;
        mStatus.baseAgility = mStatus.Agility;
    }

    private void SetSecondaryStatus()
    {
        mStatus.Dodge = (mStatus.Agility * 0.5f) + (mStatus.Luck * 0.25f);
        mStatus.Accuracy = (mStatus.Agility * 0.75f) + (mStatus.Stamina * 0.1f) + (mStatus.Strength * 0.2f);
        mStatus.Resilience = (mStatus.Stamina * 0.6f) + (mStatus.Strength * 0.4f);
        mStatus.baseDodge = mStatus.Dodge;
        mStatus.baseAccuracy = mStatus.Accuracy;
        mStatus.baseResilience = mStatus.Resilience;
    }

    private void SetSkills()
    {
        mSkills = new List<Skill>();
        mSkills.Add(new HeavyAttack());
        mSkills.Add(new LongCut());
        mSkills.Add(new ThrowRock());
        mSkills.Add(new Bomb());
       

    }
   
    [Serializable]
    public class Status {
        [Header("Main Status")]
        public int Move = 5;
        public int Health;        
        public int maxHealth;
        public int Focus;
        public int maxFocus;
        public int Stamina;        
        public int Strength;
        public int Agility;
        public int Defense;
        public int Luck;

        public int baseStrength;
        public int baseStamina;
        public int baseDefense;
        public int baseAgility;
        public int baseLuck;
        [Header("Secondary Status")]       
        public float Dodge;
        public float Accuracy;
        public float Resilience;

        public float baseDodge;
        public float baseAccuracy;
        public float baseResilience;
        
        
    }   
}
