﻿using System;
using UnityEngine;


[Serializable]
public class Weapon
{
    [Header("Main Status")]
    public string Name;
    public int Damage;
    public int Accuracy;
    public int Range;
    [Header("Special Status")]
    public float CritMultiplier;
    public float BonusDamage;
    public float BonusAccuracy;
}
