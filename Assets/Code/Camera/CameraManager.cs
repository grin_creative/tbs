﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	public Transform[] cameraPoints;
    public Transform cameraTrack;
	public int currentCameraPoint = 0;
    public float minZoom = 30;
    public int maxZoom = 12;
    public int minSideView = 5;
    public int maxSideView = 30;
    public int minSideViewF = 5;
    public int maxSideViewF = 30;
    public float moveSpeed = 0.01f;
    public float zoomSpeed = 0.02f;
    public bool Orbitate = false;
    public float orbitSideSpeed = 2f;
    public float orbitZoomSpeed = 2f;
    public float orbitMoveSpeed = 2f;
    public float orbitateMultiplier = 2f;
    public float currentZoom;

    void Start()
    {
        if (Orbitate)
        {
            currentZoom = transform.GetChild(0).localPosition.y;
            CameraOrbitateLeft();  
        }
    }


    void Update()
    {
        if (Orbitate)
        {
            OrbitateMode();
            
        }
        else
        {        
            FixedPositions();
        }

    }

    private void OrbitateMode()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            CameraOrbitateLeft();
            Debug.Log("1");
        }
        if (Input.GetKey(KeyCode.E))
        {
            CameraOrbitateRight();
        }
        if (Input.GetKey(KeyCode.R))
        {

            OrbiteCameraZoom(1);
        }
        if (Input.GetKey(KeyCode.F))
        {

            OrbiteCameraZoom(-1);
        }
        if (Input.GetKey(KeyCode.D))
        {
            OrbiteCameraSideF(1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            OrbiteCameraSideF(-1);
        }
        if (Input.GetKey(KeyCode.W))
        {

            OrbiteCameraSide(-1);
        }
        if (Input.GetKey(KeyCode.S))
        {

            OrbiteCameraSide(1);
        }
    }

    private void FixedPositions()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (currentCameraPoint == 0)
            {
                currentCameraPoint = 3;
            }
            else
            {
                currentCameraPoint--;
            }
            AdjustCameraPoint();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (currentCameraPoint == 3)
            {
                currentCameraPoint = 0;
            }
            else
            {
                currentCameraPoint++;
            }
            AdjustCameraPoint();
        }
        if (Input.GetKey(KeyCode.R))
        {

            AdjustCameraZoom(zoomSpeed);
        }
        if (Input.GetKey(KeyCode.F))
        {

            AdjustCameraZoom(-zoomSpeed);
        }
        if (Input.GetKey(KeyCode.W))
        {
            AdjustCameraSideF(moveSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            AdjustCameraSideF(-moveSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {

            AdjustCameraSide(-moveSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {

            AdjustCameraSide(moveSpeed);
        }
    }

    public void AdjustCameraPoint(){
		float oldZoom = Camera.main.transform.localPosition.z;
		Camera.main.transform.SetParent(cameraPoints[currentCameraPoint]);
		Camera.main.transform.localEulerAngles = Vector3.zero;
		Camera.main.transform.localPosition = new Vector3(0,0,0);
	}

	public void AdjustCameraZoom(float value){	

		Vector3 pos = cameraTrack.localScale;
		pos.y = Mathf.Clamp(pos.y+value, minZoom, maxZoom);
        cameraTrack.localScale = pos;
        currentZoom = transform.GetChild(0).localPosition.y;
    }

	public void AdjustCameraSide(float value){	

		Vector3 pos = cameraTrack.localScale;
		pos.x = Mathf.Clamp(pos.x - value, 0, 1);
        pos.z = Mathf.Clamp(pos.z + value, 0, 1);
        cameraTrack.localScale = pos;
	}

    public void AdjustCameraSideF(float value)
    {

        Vector3 pos = cameraTrack.localScale;
        pos.z = Mathf.Clamp(pos.z - value, 0, 1);
        pos.x = Mathf.Clamp(pos.x - value, 0, 1);
        cameraTrack.localScale = pos;
    }

    public void OrbiteCameraZoom(int value)
    {

        
        transform.GetChild(0).Translate((Vector3.forward * orbitZoomSpeed * value) * Time.deltaTime);
        if(transform.GetChild(0).localPosition.y > 12 || transform.GetChild(0).localPosition.y < 4)
        {            
            transform.GetChild(0).Translate((-Vector3.forward * orbitZoomSpeed * value) * Time.deltaTime);
        }
        currentZoom = transform.GetChild(0).localPosition.y;
    }

    public void OrbiteCameraSide(int value)
    {       
        transform.Translate((transform.GetChild(0).right * orbitSideSpeed * value) * Time.deltaTime);
        Vector3 pos = transform.position;
        pos.y = 0;
        transform.position = pos;
    }

    public void OrbiteCameraSideF(int value)
    {     
        Debug.Log(transform.GetChild(0).forward);
        transform.Translate((transform.GetChild(0).forward * orbitSideSpeed * value) * Time.deltaTime);
        Vector3 pos = transform.position;        
        pos.y = 0;
        transform.position = pos;
    }

    public void CameraOrbitateRight()
    {

        transform.GetChild(0).RotateAround(transform.position, Vector3.up, orbitMoveSpeed * Time.deltaTime);
        Vector3 normalized = (transform.GetChild(0).position - transform.position).normalized; 
        var desiredPosition = normalized * (currentZoom * (1 / normalized.y)) + (transform.position);
        Vector3 newPos = Vector3.Lerp(transform.GetChild(0).position, desiredPosition, Time.deltaTime * 2);       
        transform.GetChild(0).position = newPos;
    }

    public void CameraOrbitateLeft()
    {
       
        transform.GetChild(0).RotateAround(transform.position, -Vector3.up, orbitMoveSpeed * Time.deltaTime);
       
        Vector3 normalized = (transform.GetChild(0).position - transform.position).normalized;
        Debug.Log(normalized);
        var desiredPosition = normalized * (currentZoom * (1 / normalized.y)) + (transform.position);        
        Vector3 newPos = Vector3.Lerp(transform.GetChild(0).position, desiredPosition, Time.deltaTime * 2);       
        transform.GetChild(0).position = newPos;
    }
}
