﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapCreator : MonoBehaviour {

	enum BuildMode {Basic, Roof, HalfCircle, StairHorizontal, StairVertical};
	public Transform mTile;
	public Transform DefaultTileBox;	
	public Transform TestCharacter;
	public static Tile[,] myMap;
	public static int mapSize = 25;
    public List<Weapon> myWeapons = new List<Weapon>();  
	// Use this for initialization
	void Start () {
		FirstPass();
		BuildHouse(4,4,true);
		Color[] c = new Color[3];
		c[0] = Color.grey;		
		BuildBox(3,3,3,17,17,c, BuildMode.HalfCircle);
        CreateWeapons();
    }

    public void CreateCharacters(Transform t)
    {
        var currentSide = t.GetChild(2);
        var currentNumber = int.Parse(currentSide.GetChild(1).GetComponent<Dropdown>().captionText.text);
        int counter = 0;
        IntVector2 pos = new IntVector2(10, 10);
        foreach (Transform container in currentSide.GetChild(2))
        {
            Debug.Log(currentSide.GetChild(2).name);
            if (counter < currentNumber)
            {
                CreateCharacter(pos, container.GetChild(0).GetComponent<Dropdown>().captionText.text);
            }
            counter++;
            pos.x++;
        }
        currentSide = t.GetChild(3);
        currentNumber = int.Parse(currentSide.GetChild(1).GetComponent<Dropdown>().captionText.text);
        counter = 0;
        pos = new IntVector2(20 , 20);
        foreach (Transform container in currentSide.GetChild(2))
        {
            if (counter < currentNumber)
            {
                CreateCharacter(pos, container.GetChild(0).GetComponent<Dropdown>().captionText.text,true);
            }
            counter++;
            pos.x++;
        }
        t.gameObject.SetActive(false);
        GetComponent<TurnManager>().StartGame();

    }

    public void adjustDeployScreen(Transform t)
    {

        int max = int.Parse(t.GetChild(1).GetComponent<Dropdown>().captionText.text);
        int counter = 0;
        Debug.Log(t.GetChild(2).name);
        foreach (Transform container in t.GetChild(2))
        {
            if (counter < max)
            {
                container.gameObject.SetActive(true);
            }
            else
            {
                container.gameObject.SetActive(false);
            }
            counter++;
        }
    }


    private void CreateWeapons()
    {
        CreateTempWeapon("Sword", 1);
        CreateTempWeapon("Spear", 2);
        CreateTempWeapon("Bow", 5);
        CreateTempWeapon("Crossbow", 4);
        CreateTempWeapon("Longbow", 6);
        CreateTempWeapon("Rifle", 8);
    }

    private void CreateTempWeapon(string name, int range)
    {
        var mWeapon = new Weapon();
        mWeapon.Name = name;
        mWeapon.Damage = UnityEngine.Random.Range(2, 16);
        mWeapon.Accuracy = UnityEngine.Random.Range(50, 80);
        mWeapon.Range = range;
        mWeapon.CritMultiplier = UnityEngine.Random.Range(2, 4);
        mWeapon.BonusDamage = UnityEngine.Random.Range(0, 2);
        mWeapon.BonusAccuracy = UnityEngine.Random.Range(0, 15);
        myWeapons.Add(mWeapon);

    }


    void FirstPass(){
	Vector3 pos = Vector3.zero;
	myMap = new Tile[mapSize,mapSize];
		for (int i = 0; i < mapSize; i++){
			for(int j = 0; j < mapSize; j++){				
				Transform tempTile = Instantiate(mTile).transform;
				tempTile.position = pos;
                tempTile.name = pos.z + " " + pos.x;
                pos.z += 1; 
				myMap[i,j] = tempTile.GetComponent<Tile>();
				tempTile.GetComponent<Tile>().myPos = new IntVector2(i,j);
				tempTile.parent = transform;
				tempTile.GetComponent<Tile>().AddTileBox(DefaultTileBox);
				
			}
			pos.z = 0;
			pos.x += 1;
			
		}
	}

	void BuildBox(int heightSizeY, int widthSizeX, int widthSizeZ, int  buildPosX, int buildPosY, Color[] color, BuildMode myMode = BuildMode.Basic){
		int temp = 0;		
		while(temp != heightSizeY){
			for (int i = 0; i < widthSizeX; i++){
				for(int j = 0; j < widthSizeZ; j++){
					if(temp == 0){					
						myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().RemoveGroundTile();
					}
					if (myMode == BuildMode.HalfCircle && temp > heightSizeY-3){
						if(temp == heightSizeY-2){
								if((j == 0 && (i==0 || i == widthSizeX-1) || j == widthSizeZ-1 && (i == 0 || i == widthSizeX-1))){
									
								}							
								else {
									myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().AddTileBox(DefaultTileBox);
									myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().PaintBox(color[0]);
								}	
							}	
						if(temp == heightSizeY-1){
								if(j == 0 || j == widthSizeZ-1 || i == 0 || i == widthSizeX-1){
									
								}							
								else {
									myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().AddTileBox(DefaultTileBox);
									myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().PaintBox(color[0]);
								}	
							}										
					}
					else if(i == 0 || i == widthSizeX-1 || j == 0 || j == widthSizeZ-1 || temp == heightSizeY-1){
						if(myMode == BuildMode.Basic){
							myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().AddTileBox(DefaultTileBox);
							myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().PaintBox(color[0]);
						}
						else if(myMode == BuildMode.Roof){
							myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().AddTileBox(DefaultTileBox);
							if(i == 0 && j == 0 || i == 0 && j == widthSizeZ-1 || i == widthSizeX -1 && j == 0 || i == widthSizeX -1 && j == widthSizeZ -1){
								myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().PaintBox(color[0]);
							}
							else{
								myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().PaintBox(color[1]);
							}
						}						
						else if(myMode == BuildMode.StairVertical || myMode == BuildMode.StairHorizontal || myMode ==  BuildMode.HalfCircle){		
							myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().AddTileBox(DefaultTileBox);
							myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().PaintBox(color[0]);
						}
					}
										
					else{
						myMap[buildPosX+i,buildPosY+j].GetComponent<Tile>().ElevateGround();
					}
					
				}
			}
			temp++;
			if(myMode == BuildMode.StairHorizontal){
				widthSizeX--;
			}
			else if(myMode == BuildMode.StairVertical){
				widthSizeZ--;
			}	
		}
	}
	
	void CreateCharacter(IntVector2 pos, string weaponName, bool AI = false){
        Transform t = Instantiate(TestCharacter);
        t.position = new Vector3(pos.x, 0.3f, pos.y);
        t.GetComponent<Character>().myPos = new IntVector2(pos.x, pos.y);
        myMap[pos.x, pos.y].passable = false;
        foreach (Weapon w in myWeapons)
        {
            if (w.Name == weaponName)
            {
                t.GetComponent<Character>().mWeapon = w;
                break;
            }
        }
        GetComponent<TurnManager>().allCharacters.Add(t.GetComponent<Character>());
        if (AI)
        {
            t.GetComponent<Character>().mSide = 1;
            t.gameObject.AddComponent<AIBehaviorAggressive>();
        }

	}

	void BuildHouse(int houseSize, int houseStartPos,  bool sideBuilding = false){

		Color[] c = new Color[3];
		c[0] = Color.blue;	
		BuildBox(5,houseSize,houseSize,houseStartPos,houseStartPos,c);
		c[0] = Color.cyan;
		c[1] = Color.yellow;
		BuildBox(1,houseSize,houseSize,houseStartPos,houseStartPos,c, BuildMode.Roof);
		c[0] = Color.red;	
		BuildBox(1,houseSize-2,houseSize-2,houseStartPos+1,houseStartPos+1,c);
		c[0] = Color.magenta;
		BuildBox(2,1,1,houseStartPos+houseSize,houseStartPos+(houseSize/2-1),c);

		if(sideBuilding == true){
			BuildBox(4,3,2,houseStartPos,houseStartPos+houseSize,c, BuildMode.StairVertical);
			c[0] = Color.grey;	
		}
	}
}
