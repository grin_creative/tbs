﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CombatManager : MonoBehaviour {

    public LineRenderer myTrail;

    public void ProjectAttack(Character attacker, Character defender, Action<AttackBriefing> Callback)
    {
        if(attacker.mWeapon.Range == 1)
        {
            Callback(MeleeAttackBriefing(attacker,defender));
        }
        else
        {
            Callback(RangedAttackBriefing(attacker,defender));
        }
        
    }

    public void ProjectSkill(string Skill, Character attacker, Character defender, Action<AttackBriefing> Callback)
    {        
            var skill = attacker.mSkills.FirstOrDefault(o => o.name == Skill);
            Callback(skill.BriefingSkill(attacker,defender)); 
    }

    public void ProjectSkill(string Skill, Character attacker, Tile tile, Action<AttackBriefing> Callback)
    {        
            var skill = attacker.mSkills.FirstOrDefault(o => o.name == Skill);
            Callback(skill.BriefingSkill(attacker, null, tile)); 
    }



    public AttackBriefing ProjectAttackAI(Character attacker, Character defender)
    {
        if (attacker.mWeapon.Range == 1)
        {
            return MeleeAttackBriefing(attacker, defender);
        }
        else
        {
            return RangedAttackBriefing(attacker, defender);
        }

    }

    AttackBriefing MeleeAttackBriefing(Character attacker, Character defender) {
        AttackBriefing briefing = new AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        briefing.hitChance = attacker.mStatus.Accuracy + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge;
        briefing.minDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 0.25f);
        briefing.maxDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 1.25f);
        briefing.critChance = (attacker.mStatus.Luck * 3) + (attacker.mStatus.Accuracy * 2) - (defender.mStatus.Luck * 3);
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        return briefing;
    }

    AttackBriefing RangedAttackBriefing(Character attacker, Character defender) {
        AttackBriefing briefing = new AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        
        int dist = GridBasedPathfinding.DistanceTo(attacker.myPos, defender.myPos) - 1;
        briefing.hitChance = attacker.mStatus.Accuracy + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge - (3 * dist);
        briefing.minDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Agility * 0.25f);
        briefing.maxDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Agility * 1.25f);
        briefing.critChance = (attacker.mStatus.Luck * 3) + (attacker.mStatus.Accuracy * 1.5f) - (defender.mStatus.Luck * 3);
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        if (CalculateTrajectory(dist, attacker, defender))
        {
            briefing.hitChance = 0;
        }
        return briefing;
    }

    AttackBriefing DamageSkillBriefing(Character attacker, Character defender)
    {
        AttackBriefing briefing = new AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        briefing.hitChance = attacker.mStatus.Accuracy + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge;
        briefing.minDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 0.25f);
        briefing.maxDamage = attacker.mWeapon.Damage + (int)attacker.mWeapon.BonusDamage + (int)(attacker.mStatus.Strength * 1.25f);
        briefing.critChance = (attacker.mStatus.Luck * 3) + (attacker.mStatus.Accuracy * 2) - (defender.mStatus.Luck * 3);
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        return briefing;
    }

    AttackBriefing EffectSkillBriefing(Character attacker, Character defender)
    {
        AttackBriefing briefing = new AttackBriefing();
        briefing.attacker = attacker;
        briefing.defender = defender;
        briefing.hitChance = attacker.mStatus.Accuracy + attacker.mWeapon.Accuracy + attacker.mWeapon.BonusAccuracy - defender.mStatus.Dodge;     
        briefing.critMultiplier = attacker.mWeapon.CritMultiplier;
        briefing.enemyHealth = defender.mStatus.Health;
        return briefing;
    }


    bool CalculateTrajectory(int dist, Character attacker, Character defender)
    {
        
        List<Vector3> points = new List<Vector3>();
        Vector3 p = attacker.transform.position;
        p.y += 0.5f;
        points.Add(p);
        if (dist > 3 && attacker.mWeapon.Name != "Crossbow" && attacker.mWeapon.Name != "Rifle")
        {
            points.Add(new Vector3((attacker.transform.position.x + defender.transform.position.x) / 2, 4, (attacker.transform.position.z + defender.transform.position.z) / 2));
        }
        p = defender.transform.position;
        p.y += 0.5f;
        points.Add(p);
        points = Curver.MakeSmoothCurve(points, 5.0f);
        points.RemoveAt(0);        
        points.RemoveAt(points.Count - 1);
        myTrail.positionCount = points.Count;
        int counter = 0;
        bool collision = false;
        foreach (var i in points)
        {
            if (counter < points.Count - 1) {
                Debug.Log("gg");
                if (Physics.Linecast(points[counter], points[counter+1]))
                {                   
                    collision = true;
                }
            }
            myTrail.SetPosition(counter, i);
            ++counter;
        }
        return collision;

    }    

    public void Attack(AttackBriefing b, Action<List<DamageReport>> Callback)
    {
        myTrail.positionCount = 0;
        int dmg = 0;
        if (UnityEngine.Random.Range(1,100) <= b.hitChance)
        {
            int value = 1;
            if (UnityEngine.Random.Range(1, 100) <= b.critChance)
            {
                value = (int)b.critMultiplier;
            }
            dmg = value * UnityEngine.Random.Range(b.minDamage, b.maxDamage);
            b.defender.mStatus.Health -= dmg;

        }
        else
        {
            dmg = -1;          
        }
        var list = new List<DamageReport>();
        if(b.defender.mStatus.Health <= 0)
        {
            list.Add(new DamageReport(true, dmg, b.defender));
            Callback(list);
            
        }
        else
        {
            list.Add(new DamageReport(false, dmg, b.defender));
            Callback(list);            
        } 
       
    }  

     public void Skill(string Skill,AttackBriefing b, Action<List<DamageReport>> Callback)
    {   
        var skill = b.attacker.mSkills.FirstOrDefault(o => o.name == Skill);
        Callback(skill.UseSkill(myTrail,b));         
    }  

    public class AttackBriefing
    {
        public float hitChance = 0;
        public float critChance = 0;
        public float critMultiplier = 0;
        public int minDamage = 0;
        public int maxDamage = 0;
        public int enemyHealth = 0;
        public Character attacker;
        public Character defender;

        public bool skill = false;
    }

    public class DamageReport
    {
        public DamageReport(bool b, int d, Character c){
            death = b;
            dmg = d;
            character = c;
        }
        public bool death;
        public int dmg;
        public Character character;
    }

    
}
