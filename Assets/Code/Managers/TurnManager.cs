﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TurnManager : MonoBehaviour
{

    public CustomStandaloneInputModule myModule;
    public CombatManager mCombatManager;
    public CombatManager.AttackBriefing currentBriefing;

    public Transform myCanvas;
    public Transform skillSubMenu;

    public Character selectedCharacter;
    public List<Character> allCharacters = new List<Character>();

    public Text Header;
    public RectTransform[] dynamicText;

    public int currentTurn = 0;
    public int totalTurns = 1;
    //DynamicTextValues
    public float dynamicTextX = 0;
    public float dynamicTextY = 0;
    public float dAdjustX = 0;
    public float dAdjustY = 0;
    //EndDynamicTextValues

    public enum ActiveMode { MovementMode, ActionMode, SelectedMode, FreeMode, EnemyMode };

    //ActionWindowValues
    public float x = 1;
    public float y = 1;
    public float yInversed = 0.04f;

    public bool skillInverse = false;
    //EndActionWindowValues

    public int skill = 0;
    string skillName = "";
    public bool doubleClick = false;
    public bool cMoving = false;

    int xRef = 1920;
    int yRef = 1080;
    [SerializeField]
    ActiveMode mModes = new ActiveMode();

    // Use this for initialization
    public void StartGame()
    {
        myCanvas = GameObject.Find("Canvas").transform;
        Header = myCanvas.Find("Header").GetChild(0).GetComponent<Text>();
        mModes = ActiveMode.FreeMode;
        allCharacters.OrderBy(c => c.mStatus.Agility);
        int order = 1;
        foreach (Character c in allCharacters)
        {
            c.mTurn = order;
            order++;
        }
        selectedCharacter = allCharacters[0];
        selectedCharacter.transform.Find("Quad").gameObject.SetActive(true);
        skillSubMenu.GetComponent<AdjustGrit>().Adjust();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            TestGrid();
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (mModes == ActiveMode.FreeMode)
            {
                FreeModeActions();
            }
            else if (mModes == ActiveMode.ActionMode)
            {
                ActionModeActions();
            }
            else if (mModes == ActiveMode.MovementMode)
            {
                MovementModeActions();
            }
            else if (mModes == ActiveMode.SelectedMode)
            {
                SelectedModeActions();
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            if (mModes == ActiveMode.FreeMode)
            {
                GridBasedPathfinding.instance.CleanHighlightedArea();
                GridBasedPathfinding.instance.BlueHighlightedArea();
            }
            else if (mModes == ActiveMode.MovementMode)
            {

                mModes = ActiveMode.FreeMode;
                AdjustActionWindow();
                GridBasedPathfinding.instance.CleanHighlightedArea();
                GridBasedPathfinding.instance.BlueHighlightedArea();
            }
            else if (mModes == ActiveMode.ActionMode)
            {

                mModes = ActiveMode.FreeMode;
                HideCombatPreview();
                AdjustActionWindow();
                GridBasedPathfinding.instance.CleanHighlightedArea();
                GridBasedPathfinding.instance.BlueHighlightedArea();
            }
        }
    }

    void TestGrid()
    {

    }

    public void FreeModeActions()
    {
        // try
        //  {
        if(myModule.GetPointerData().pointerEnter != null){
        GameObject clicked = myModule.GetPointerData().pointerEnter;

        if (clicked.tag == "Character" && selectedCharacter.mTurn == clicked.transform.parent.GetComponent<Character>().mTurn)
        {
          
            AdjustActionWindow();
        }
        else if (clicked.tag == "Character" && clicked.transform.parent.GetComponent<Character>().mSide > 0)
        {
            GridBasedPathfinding.instance.CalculateMovementZone(clicked.transform.parent.GetComponent<Character>(), true);
        }
        else if (clicked.tag == "UI")
        {

        }
        else if (clicked.tag != "Character")
        {

            Debug.Log(myModule.GetPointerData().pointerEnter.gameObject.tag);

            HideActionWindow();
        }
        }
        // }
        // catch
        // {

        // }
    }

    public void MovementModeActions()
    {
        // try
        // {             
        if (myModule.GetPointerData().pointerEnter.gameObject.tag == "Tile")
        {
            Tile t = myModule.GetPointerData().pointerEnter.gameObject.transform.parent.GetComponent<Tile>();
            if (t.confirmMovement == true)
            {
                doubleClick = !doubleClick;
                if (doubleClick == false && cMoving == false)
                {
                    Debug.Log("hoy");
                    t.confirmMovement = false;
                    cMoving = true;
                    MoveCharacter();
                }
            }
            else if (t.highlighted == true && cMoving == false)
            {
                FindRoute(t);
            }
        }




        //  }
        // catch
        //  {

        //  }
    }

    public void ActionModeActions()
    {
        //  try
        // {
        if(myModule.GetPointerData().pointerEnter != null){
        GameObject clicked = myModule.GetPointerData().pointerEnter;
        Debug.Log(clicked.tag);
        if (clicked.tag == "Character")
        {

            Character clickedCharacter = myModule.GetPointerData().pointerEnter.gameObject.transform.parent.GetComponent<Character>();

            if (clickedCharacter.mSide == 1 && MapCreator.myMap[clickedCharacter.myPos.x, clickedCharacter.myPos.y].highlighted)
            {
                if (skill == 1)
                {
                    mCombatManager.ProjectSkill(skillName, selectedCharacter, clickedCharacter, GetCombatPreview);
                }                
                else
                {
                    mCombatManager.ProjectAttack(selectedCharacter, clickedCharacter, GetCombatPreview);
                }

            }
        }
        else if (skill == 2 && (clicked.tag == "Tile" || clicked.tag == "Character")){
            var clickedTile = myModule.GetPointerData().pointerEnter.gameObject.transform.parent.GetComponent<Tile>();
            if(clickedTile.highlighted){
                mCombatManager.ProjectSkill(skillName, selectedCharacter, clickedTile, GetCombatPreview);
                
            }
                
        }
        else if (clicked.tag == "UI")
        {         
        }
        else
        {
            HideCombatPreview();
        }
        }

        // }
        //  catch
        //   {

        //  }
    }

    public void SelectedModeActions()
    {
        try
        {

        }
        catch
        {

        }
    }

    public void MoveCharacter()
    {
        GridBasedPathfinding.instance.CallMoveCharacter(MoveFinished);
    }

    public void MoveFinished()
    {
        AdjustActionWindow();
        cMoving = false;
    }

    public void CalculateArea()
    {
        mModes = ActiveMode.MovementMode;
        myCanvas.Find("ActionWindow").gameObject.SetActive(false);
        GridBasedPathfinding.instance.CalculateMovementZone(selectedCharacter);

    }

    public void CalculateRange()
    {
        mModes = ActiveMode.ActionMode;
        myCanvas.Find("ActionWindow").gameObject.SetActive(false);
        GridBasedPathfinding.instance.AttackRange(selectedCharacter);
    }
    // Update is called once per frame

    public void FindRoute(Tile t)
    {

        IntVector2 destinationPoint = new IntVector2(int.Parse(t.transform.position.x.ToString()), int.Parse(t.transform.position.z.ToString()));
        var watch = System.Diagnostics.Stopwatch.StartNew();
        GridBasedPathfinding.instance.FindShortestRouteInitial(selectedCharacter, destinationPoint, selectedCharacter.Move, selectedCharacter.Height);
        watch.Stop();


    }



    void CheckPossibleActions(Transform t)
    {
        if (selectedCharacter.mAction)
        {
            t.GetChild(1).GetComponent<Button>().interactable = false;
            t.GetChild(2).GetComponent<Button>().interactable = false;
        }
        else
        {
            t.GetChild(1).GetComponent<Button>().interactable = true;
            t.GetChild(2).GetComponent<Button>().interactable = true;
        }

        if (selectedCharacter.mMoved)
        {
            t.GetChild(0).GetComponent<Button>().interactable = false;
        }
        else
        {
            t.GetChild(0).GetComponent<Button>().interactable = true;
        }
    }

    #region  //Action Window Methods

    public void HideActionWindow()
    {
        RectTransform ActionWindow = (RectTransform)myCanvas.Find("ActionWindow");
        ActionWindow.gameObject.SetActive(false);
        skillInverse = false;
        skillSubMenu.parent.gameObject.SetActive(false);
    }

    public void AdjustActionWindow()
    { 
        RectTransform ActionWindow = (RectTransform)myCanvas.Find("ActionWindow");
        CheckPossibleActions(ActionWindow);
        Vector3 v = Curver.AdjustMenusByScreenBounds(xRef,yRef,x,y,selectedCharacter,ActionWindow,yInversed);    
        ActionWindow.position = v;
        ActionWindow.gameObject.SetActive(true);
    }

    public void EndTurn()
    {
        selectedCharacter.mAction = false;
        selectedCharacter.mMoved = false;
        selectedCharacter.Move = selectedCharacter.mStatus.Move;
        selectedCharacter.transform.Find("Quad").gameObject.SetActive(false);
        Header.transform.parent.gameObject.SetActive(false);
        HideActionWindow();
        mModes = ActiveMode.FreeMode;
        currentTurn++;
        totalTurns++;
        if (currentTurn >= allCharacters.Count)
        {
            currentTurn = 0;
        }
        selectedCharacter = allCharacters[currentTurn];
        selectedCharacter.transform.Find("Quad").gameObject.SetActive(true);
        if (selectedCharacter.mSide > 0)
        {
            mModes = ActiveMode.EnemyMode;
            selectedCharacter.GetComponent<AIBehaviorAggressive>().Init(allCharacters, selectedCharacter, Header);
            Header.transform.parent.gameObject.SetActive(true);
            Header.text = "Enemy Turn - Thinking";
        }
    }

    #endregion

    #region //Combat Window Methods

    public void GetCombatPreview(CombatManager.AttackBriefing attBrief)
    {

        currentBriefing = attBrief;
        Transform stats = myCanvas.Find("CombatWindow").GetChild(1);
        stats.GetChild(0).GetChild(0).GetComponent<Text>().text = attBrief.enemyHealth.ToString();
        stats.GetChild(1).GetChild(0).GetComponent<Text>().text = attBrief.hitChance.ToString();
        stats.GetChild(2).GetChild(0).GetComponent<Text>().text = attBrief.minDamage + " to " + attBrief.maxDamage;
        stats.GetChild(3).GetChild(0).GetComponent<Text>().text = attBrief.critChance.ToString();
        stats.GetChild(4).GetChild(0).GetComponent<Text>().text = attBrief.critMultiplier.ToString();
        myCanvas.Find("CombatWindow").gameObject.SetActive(true);

    }

    public void InitiateAttack()
    {
        if (skill > 0)
        {
            mCombatManager.Skill(skillName, currentBriefing, EndAttack);
        }
        else
        {
            mCombatManager.Attack(currentBriefing, EndAttack);
        }
    }

    public void EndAttack(List<CombatManager.DamageReport> report)
    {
        bool b = false;
        int count = 0;
        foreach (var rep in report)
        {
            StartCoroutine(ShowDamage(count, rep));
            if (rep.death)
            {
                b = true;
            }       
            count++;      
        }
        if (b)
        {

            RemoveDeadCharacters();
            if(selectedCharacter.mSide == 1)
            selectedCharacter.mMoved = false;
        }
        else
        {
            if(selectedCharacter.mSide == 1)
            selectedCharacter.mMoved = true;
        }
        
        selectedCharacter.mAction = true;
        HideCombatPreview();
        GridBasedPathfinding.instance.CleanHighlightedArea();
        GridBasedPathfinding.instance.BlueHighlightedArea();
        if (selectedCharacter.mSide == 0)
        {
            AdjustActionWindow();
        }
        skill = 0;
        skillSubMenu.parent.gameObject.SetActive(false);
        skillName = "";
    }

    IEnumerator ShowDamage(int c, CombatManager.DamageReport report)
    {
        Vector3 location = report.character.transform.position;
        var realOffset = report.character.transform.up * dynamicTextY;
        realOffset += report.character.transform.right * Random.Range(-dynamicTextX, dynamicTextX);
        //location += realOffset;
        // location.x += 1f;//Random.Range(0.2f,1f);
        Camera.main.ViewportToScreenPoint(location);
        Vector3 v = Camera.main.WorldToScreenPoint(location);
        v += realOffset;
        float adjustmentX = (Camera.main.pixelRect.width - xRef) / xRef;
        float adjustmentY = (Camera.main.pixelRect.height - yRef) / yRef;
        v.x += dynamicText[c].sizeDelta.x * (dAdjustX - (dAdjustX * Mathf.Abs(adjustmentX)));
        v.y += dynamicText[c].sizeDelta.y * (dAdjustY - (dAdjustY * Mathf.Abs(adjustmentY)));
        dynamicText[c].position = v;
        if (report.dmg == -1)
        {
            dynamicText[c].GetComponent<Text>().text = "Miss";
        }
        else
        {
            dynamicText[c].GetComponent<Text>().text = report.dmg + "";
        }
        dynamicText[c].gameObject.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        dynamicText[c].gameObject.SetActive(false);
    }

    public void ShowSkills()
    {
        bool b = false;
        foreach (Transform t in skillSubMenu)
        {
            if (b == false)
            {
                b = true;
            }
            else
            {
                Destroy(t.gameObject);
            }
        }
        foreach (var skill in selectedCharacter.mSkills)
        {
            Transform t = Instantiate(skillSubMenu.GetChild(0));
            t.SetParent(skillSubMenu);
            t.gameObject.SetActive(true);
            t.localScale = new Vector3(1, 1, 1);
            t.GetChild(0).GetComponent<Text>().text = skill.name;
            t.GetComponent<Button>().onClick.AddListener(delegate { prepareSkill(skill.name, skill.type); });
            skillSubMenu.GetComponent<AdjustGrit>().Adjust();
        }
        
        Vector3 v = Curver.AdjustSubMenusByScreenBoundsAdditive(xRef,yRef,x,y,skillSubMenu.parent.parent.Find("Waypoint").GetComponent<RectTransform>(),skillSubMenu.parent.GetComponent<RectTransform>(),yInversed);    
        skillSubMenu.parent.GetComponent<RectTransform>().position = v;
        skillSubMenu.parent.gameObject.SetActive(true);
        Debug.Log("test");
    }

    void prepareSkill(string name, Enums.skillType type)
    {
        Debug.Log("test");
        if(Enums.skillType.SingleDamage == type){
            skill = 1;
        }
        else {
            skill = 2;
        }        
        skillName = name;
        mModes = ActiveMode.ActionMode;
        myCanvas.Find("ActionWindow").gameObject.SetActive(false);
        GridBasedPathfinding.instance.SkillRange(name, selectedCharacter);
    }

    public void RemoveDeadCharacters()
    {
        List<Character> cL = new List<Character>();
        foreach (var v in allCharacters)
        {
            if (v.mStatus.Health <= 0)
            {
                MapCreator.myMap[v.myPos.x, v.myPos.y].passable = true;
                Destroy(v.gameObject);
            }
            else
            {
                cL.Add(v);
            }
        }
        allCharacters = cL;
    }

    public void HideCombatPreview()
    {
        currentBriefing = null;
        myCanvas.Find("CombatWindow").gameObject.SetActive(false);
        GetComponent<CombatManager>().myTrail.positionCount = 0;
    }

    #endregion

}
