﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GridBasedPathfinding : MonoBehaviour {
    public int calc = 0;
    public int deadEnd = 3;
    private bool pathFound = false;
    public Character selectedCharacter;
    public List<IntVector2> shortestRoute = new List<IntVector2>();
    // public List<IntVector2> currentRoute = new List<IntVector2>();
    public int currentShortMove = 0;
    public HashSet<IntVector2> highLightedArea = new HashSet<IntVector2>();

    public List<Tile> AttackArea;

    public static GridBasedPathfinding instance;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void CallMoveCharacter(Action callback)
    {
        StartCoroutine(MoveCharacter2(callback));
    }

    IEnumerator MoveCharacter2(Action callback)
    {
        int temp = 0;
        foreach (var i in shortestRoute)
        {
            Vector3 v;
            int size = MapCreator.myMap[i.x, i.y].mSize - 1;
            if (size == 0)
            {
               v = new Vector3(i.x, 0.3f, i.y);
            }
            else
            {
                v = new Vector3(i.x, (0.5f * size) + 0.3f, i.y);
            }
            while (selectedCharacter.transform.position != v)
            {
                yield return new WaitForSeconds(0.03f);
                selectedCharacter.transform.position = Vector3.MoveTowards(selectedCharacter.transform.position, v, 0.1f);
            }
            temp++;
        }
        MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y].passable = true;
        selectedCharacter.myPos = selectedCharacter.desiredPos;
        MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y].passable = false;
        selectedCharacter.mMoved = true;
        selectedCharacter = null;
        callback.Invoke();
        CleanHighlightedArea();
        BlueHighlightedArea();

    }

    public void CleanSkill(){
        foreach(var v in AttackArea){
            v.Clear();
        }
        AttackArea.Clear();
    }
    public void CalculateMovementZone(Character actor, bool enemy = false)
    {
        selectedCharacter = actor;
        var watch = System.Diagnostics.Stopwatch.StartNew();
        FindPossibleTilesInitial(actor.myPos, actor.Move, actor.Height);
        watch.Stop();
       
        if (enemy == false && selectedCharacter.mSide == 0)
        {
            foreach (var v in highLightedArea)
            {
                MapCreator.myMap[v.x, v.y].highlight();
            }
        }
        else if (enemy == true)
        {
            Color32 c = new Color32(255, 0, 0, 128);
            foreach (var v in highLightedArea)
            {

                MapCreator.myMap[v.x, v.y].highlight(c);
            }
        }
        calc = 0;
    }

    void FindPossibleTilesInitial(IntVector2 vectorD, int moveP, int heightP)
    {
        MapCreator.myMap[vectorD.x, vectorD.y].passable = false;

        int move = moveP;
        int height = heightP;
        int moves = moveP;
        int heights = heightP;
        int movel = moveP;
        int heightl = heightP;
        int mover = moveP;
        int heightr = heightP;
        bool f = false;
        bool b = false;
        bool l = false;
        bool r = false;

        List<bool> tempList = new List<bool>();

        calc += 4;
        if (move > 0)
        {
            if (vectorD.x + 1 < MapCreator.mapSize)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    move--;
                    if (move > 0)
                    {
                        f = true;
                    }
                }
                else
                {
                    if (move - extraCost >= 0)
                    {
                        move -= extraCost;
                        f = true;
                    }
                }
            }


            if (vectorD.x - 1 >= 0)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    moves--;

                    if (moves > 0)
                    {
                        b = true;
                    }
                }
                else
                {
                    if (moves - extraCost >= 0)
                    {
                        moves -= extraCost;
                        b = true;
                    }
                }
            }


            if (vectorD.y + 1 < MapCreator.mapSize)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    movel--;

                    if (movel > 0)
                    {
                        l = true;
                    }
                }
                else
                {
                    if (movel - extraCost >= 0)
                    {
                        movel -= extraCost;
                        l = true;
                    }
                }
            }

            if (vectorD.y - 1 >= 0)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    mover--;
                    if (mover > 0)
                    {
                        r = true;
                    }
                }
                else
                {
                    if (mover - extraCost >= 0)
                    {
                        mover -= extraCost;
                        r = true;
                    }
                }
            }









            if (f) FindPossibleTiles(new IntVector2(selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y), move, height, 2, 1);
            if (b) FindPossibleTiles(new IntVector2(selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y), moves, heights, 1, 2);
            if (l) FindPossibleTiles(new IntVector2(selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1), movel, heightl, 4, 3);
            if (r) FindPossibleTiles(new IntVector2(selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1), mover, heightr, 3, 4);
        }
        int t = 0;
        if (vectorD.x + 1 < MapCreator.mapSize)
        {
            MapCreator.myMap[selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y].passable = tempList[t];
            t++;
        }
        if (vectorD.x - 1 >= 0)
        {
            MapCreator.myMap[selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y].passable = tempList[t];
            t++;
        }
        if (vectorD.y + 1 < MapCreator.mapSize)
        {
            MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1].passable = tempList[t];
            t++;
        }
        if (vectorD.y - 1 >= 0)
        {
            MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1].passable = tempList[t];
            t++;
        }
    }

    void FindPossibleTiles(IntVector2 vectorD, int moveP, int heightP, int lastDir = 0, int tob = 0)
    {
        if (MapCreator.myMap[vectorD.x, vectorD.y].passable == false)
        {

            return;
        }

        if (tob == 1) { MapCreator.myMap[selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y].passable = false; }
        if (tob == 2) { MapCreator.myMap[selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y].passable = false; }
        if (tob == 3) { MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1].passable = false; }
        if (tob == 4) { MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1].passable = false; }

        highLightedArea.Add(vectorD);
        int move = moveP;
        int height = heightP;
        if (move > 0)
        {
            if (vectorD.x + 1 < MapCreator.mapSize && lastDir != 1)
            {
                int extraCost = heightP - MapCreator.myMap[vectorD.x + 1, vectorD.y].mSize;
                if (extraCost <= 0)
                {
                    move--;
                    if (move > 0)
                    {
                        FindPossibleTiles(new IntVector2(vectorD.x + 1, vectorD.y), move, heightP, 2);
                    }
                }
                else
                {
                    if (move - extraCost >= 0)
                    {
                        move -= extraCost;
                        FindPossibleTiles(new IntVector2(vectorD.x + 1, vectorD.y), move, heightP, 2);
                    }
                }
            }

            move = moveP;
            height = heightP;
            if (vectorD.x - 1 >= 0 && lastDir != 2)
            {
                int extraCost = heightP - MapCreator.myMap[vectorD.x -1, vectorD.y].mSize;
                if (extraCost <= 0)
                {
                    move--;

                    if (move > 0)
                    {
                        FindPossibleTiles(new IntVector2(vectorD.x - 1, vectorD.y), move, heightP, 1);
                    }
                }
                else
                {
                    if (move - extraCost >= 0)
                    {
                        move -= extraCost;
                        FindPossibleTiles(new IntVector2(vectorD.x - 1, vectorD.y), move, heightP, 1);
                    }
                }
            }

            move = moveP;
            height = heightP;
            if (vectorD.y + 1 < MapCreator.mapSize && lastDir != 3)
            {
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    move--;

                    if (move > 0)
                    {
                        FindPossibleTiles(new IntVector2(vectorD.x, vectorD.y + 1), move, heightP, 4);
                    }
                }
                else
                {
                    if (move - extraCost >= 0)
                    {
                        move -= extraCost;
                        FindPossibleTiles(new IntVector2(vectorD.x, vectorD.y + 1), move, heightP, 4);
                    }
                }
            }
            move = moveP;
            height = heightP;
            if (vectorD.y - 1 >= 0 && lastDir != 4)
            {
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y - 1].mSize;
                if (extraCost <= 0)
                {
                    move--;
                    if (move > 0)
                    {
                        FindPossibleTiles(new IntVector2(vectorD.x, vectorD.y - 1), move, heightP, 3);
                    }
                }
                else
                {
                    if (move - extraCost >= 0)
                    {
                        move -= extraCost;
                        FindPossibleTiles(new IntVector2(vectorD.x, vectorD.y - 1), move, heightP, 3);
                    }
                }
            }
        }
    }

    public void FindShortestRouteInitial(Character actor, IntVector2 newPos, int moveP, int heightP)
    {
        MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y].passable = false;
        IntVector2 vectorD = selectedCharacter.myPos;
        int move = moveP;
        int height = heightP;
        int moves = moveP;
        int heights = heightP;
        int movel = moveP;
        int heightl = heightP;
        int mover = moveP;
        int heightr = heightP;
        bool f = false;
        bool b = false;
        bool l = false;
        bool r = false;
        List<bool> tempList = new List<bool>();
        selectedCharacter.desiredPos = newPos;
        BlueHighlightedArea();
        currentShortMove = moveP;
        if (move > 0)
        {
            if (vectorD.x + 1 < MapCreator.mapSize)
            {

                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {

                    move--;
                    if (move > 0)
                    {
                        f = true;
                    }
                }
                else
                {
                    if (move - extraCost >= 0)
                    {
                        move -= extraCost;
                        f = true;
                    }
                }
            }

            if (vectorD.x - 1 >= 0)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    moves--;

                    if (moves > 0)
                    {
                        b = true;
                    }
                }
                else
                {
                    if (moves - extraCost >= 0)
                    {
                        moves -= extraCost;
                        b = true;
                    }
                }
            }

            if (vectorD.y + 1 < MapCreator.mapSize)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    movel--;

                    if (movel > 0)
                    {
                        l = true;
                    }
                }
                else
                {
                    if (movel - extraCost >= 0)
                    {
                        movel -= extraCost;
                        l = true;
                    }
                }
            }

            if (vectorD.y - 1 >= 0)
            {
                tempList.Add(MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1].passable);
                int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                if (extraCost <= 0)
                {
                    mover--;
                    if (mover > 0)
                    {
                        r = true;
                    }
                }
                else
                {
                    if (mover - extraCost >= 0)
                    {
                        mover -= extraCost;
                        r = true;
                    }
                }
            }




            if (f) FindShortestRoute(new IntVector2(vectorD.x + 1, vectorD.y), move, heightP, new List<IntVector2>(), 2, 1);
            if (pathFound == false)
            {
                if (b) FindShortestRoute(new IntVector2(vectorD.x - 1, vectorD.y), moves, heightP, new List<IntVector2>(), 1, 2);
            }
            if (pathFound == false)
            {
                if (l) FindShortestRoute(new IntVector2(vectorD.x, vectorD.y + 1), movel, heightP, new List<IntVector2>(), 4, 3);
            }
            if (pathFound == false)
            {
                if (r) FindShortestRoute(new IntVector2(vectorD.x, vectorD.y - 1), mover, heightP, new List<IntVector2>(), 3, 4);
            }

            int t = 0;
            if (vectorD.x + 1 < MapCreator.mapSize)
            {
                MapCreator.myMap[selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y].passable = tempList[t];
                t++;
            }
            if (vectorD.x - 1 >= 0)
            {
                MapCreator.myMap[selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y].passable = tempList[t];
                t++;
            }
            if (vectorD.y + 1 < MapCreator.mapSize)
            {
                MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1].passable = tempList[t];
                t++;
            }
            if (vectorD.y - 1 >= 0)
            {
                MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1].passable = tempList[t];
                t++;
            }


            foreach (var i in shortestRoute)
            {
                MapCreator.myMap[i.x, i.y].transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;
            }
            var c = shortestRoute[shortestRoute.Count - 1];
            MapCreator.myMap[c.x, c.y].highlight(Color.magenta, true);
            pathFound = false;
            calc = 0;
        }
        //  selectedCharacter = null;

    }

    void FindShortestRoute(IntVector2 vectorD, int moveP, int heightP, List<IntVector2> mylist, int lastDir = 0, int tob = 0)
    {
        if (pathFound == false) {
            if (MapCreator.myMap[vectorD.x, vectorD.y].passable == false)
            {
                return;
            }

            var count = mylist.Count;
            if (count > selectedCharacter.Move)
            {
                return;
            }
            if (DistanceTo(vectorD, selectedCharacter.desiredPos) > moveP)
            {
                return;
            }
            List<IntVector2> currentRoute = CloneItems(mylist);
            currentRoute.Add(vectorD);

            if (shortestRoute.Count == 0 || currentRoute.Count < currentShortMove)
            {
                if (currentRoute[currentRoute.Count - 1].toVector2() == selectedCharacter.desiredPos.toVector2())
                {
                    currentShortMove = currentRoute.Count - 1;
                    shortestRoute.Clear();
                    shortestRoute.AddRange(currentRoute.ToArray());

                    if (DistanceTo(selectedCharacter.myPos, selectedCharacter.desiredPos) == shortestRoute.Count) {

                        pathFound = true;
                    }
                    return;
                }
            }

            if (tob == 1) { MapCreator.myMap[selectedCharacter.myPos.x + 1, selectedCharacter.myPos.y].passable = false; }
            if (tob == 2) { MapCreator.myMap[selectedCharacter.myPos.x - 1, selectedCharacter.myPos.y].passable = false; }
            if (tob == 3) { MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + 1].passable = false; }
            if (tob == 4) { MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y - 1].passable = false; }

            // int move = moveP;
            //  int height = heightP;
            calc++;
            var intVector2 = new IntVector2(vectorD.x + 1, vectorD.y);
            if (moveP > 0)
            {
                if (intVector2.x < MapCreator.mapSize && lastDir != 1)
                {

                    if (count < deadEnd || checkVector(intVector2, currentRoute[count - deadEnd]))
                    {
                        int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                        if (extraCost <= 0)
                        {
                            // move--;
                            if (moveP - 1 > 0)
                            {
                                FindShortestRoute(intVector2, moveP - 1, heightP, currentRoute, 2);
                            }
                        }
                        else
                        {
                            if (moveP - extraCost >= 0)
                            {
                                //  move -= extraCost;
                                FindShortestRoute(intVector2, moveP - extraCost, heightP, currentRoute, 2);
                            }

                        }
                    }
                }
                // move = moveP;
                //  height = heightP;
                intVector2 = new IntVector2(vectorD.x - 1, vectorD.y);
                if (vectorD.x - 1 >= 0 && lastDir != 2)
                {
                    if (count < deadEnd || checkVector(intVector2, currentRoute[count - deadEnd]))
                    {
                        int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                        if (extraCost <= 0)
                        {
                            // move--;
                            if (moveP - 1 > 0)
                            {
                                FindShortestRoute(intVector2, moveP - 1, heightP, currentRoute, 1);
                            }
                        }
                        else
                        {
                            if (moveP - extraCost >= 0)
                            {
                                // move -= extraCost;
                                FindShortestRoute(intVector2, moveP - extraCost, heightP, currentRoute, 1);
                            }
                        }
                    }
                }

                // move = moveP;
                //   height = heightP;
                intVector2 = new IntVector2(vectorD.x, vectorD.y + 1);
                if (vectorD.y + 1 < MapCreator.mapSize && lastDir != 3)
                {
                    if (count < deadEnd || checkVector(intVector2, currentRoute[count - deadEnd]))
                    {
                        int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                        if (extraCost <= 0)
                        {
                            //   move--;
                            if (moveP - 1 > 0)
                            {
                                FindShortestRoute(intVector2, moveP - 1, heightP, currentRoute, 4);
                            }
                        }
                        else
                        {
                            if (moveP - extraCost >= 0)
                            {
                                //move -= extraCost;
                                FindShortestRoute(intVector2, moveP - extraCost, heightP, currentRoute, 4);
                            }
                        }
                    }
                }
                //  move = moveP;
                //   height = heightP;
                intVector2 = new IntVector2(vectorD.x, vectorD.y - 1);
                if (vectorD.y - 1 >= 0 && lastDir != 4)
                {
                    if (count < deadEnd || checkVector(intVector2, currentRoute[count - deadEnd]))
                    {
                        int extraCost = heightP - MapCreator.myMap[vectorD.x, vectorD.y + 1].mSize;
                        if (extraCost <= 0)
                        {
                            //   move--;
                            if (moveP - 1 > 0)
                            {
                                FindShortestRoute(intVector2, moveP - 1, heightP, currentRoute, 3);
                            }
                        }
                        else
                        {
                            if (moveP - extraCost >= 0)
                            {
                                //     move -= extraCost;
                                FindShortestRoute(intVector2, moveP - extraCost, heightP, currentRoute, 3);
                            }
                        }
                    }
                }
            }
        }
    }

    public void CleanHighlightedArea()
    {
        foreach (var v in MapCreator.myMap)
        {
            v.Clear();
        }
        highLightedArea.Clear();
    }

    public void BlueHighlightedArea()
    {
        foreach (var v in MapCreator.myMap)
        {
            v.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = new Color32(26, 23, 118, 112);
        }
    }

    private List<IntVector2> CloneItems(List<IntVector2> itemsToClone)
    {


        int numItems = itemsToClone.Count;
        List<IntVector2> itemsToBeReturned = new List<IntVector2>(numItems);

        for (int i = 0; i < numItems; i++)
        {
            itemsToBeReturned.Add(itemsToClone[i]);
        }
        return itemsToBeReturned;

    }

    public static bool checkVector(IntVector2 a, IntVector2 b) {
        if (a.x == b.x && a.y == b.y) {
            return false;
        }
        else {
            return true;
        }

    }

    public static int VectorDistance(IntVector2 a, IntVector2 b) {
        return (a.x + a.y) - (b.x + b.y);

    }

    public static int DistanceTo(IntVector2 a, IntVector2 b)
    {
        int ia = (b.x - a.x);
        int ib = (b.y - a.y);

        return (int)Mathf.Sqrt(ia * ia) + (int)Mathf.Sqrt(ib * ib);
        // return Math.Abs((b.x-a.x) + (b.y - a.y));
    }

    public void AttackRange(Character actor)
    {
        selectedCharacter = actor;
        var watch = System.Diagnostics.Stopwatch.StartNew();
        FindPossibleAttackSpots(selectedCharacter.myPos,selectedCharacter.mWeapon.Range);
        watch.Stop();
        Debug.Log(watch.ElapsedMilliseconds + " ms");     
    }

    public void SkillRange(string Skill, Character actor)
    {
        selectedCharacter = actor;
        var skill = selectedCharacter.mSkills.FirstOrDefault(o => o.name == Skill);
        var watch = System.Diagnostics.Stopwatch.StartNew();
        Debug.Log(Skill);
        FindPossibleAttackSpots(selectedCharacter.myPos,skill.range);
        watch.Stop();
        Debug.Log(watch.ElapsedMilliseconds + " ms");     
    }

    void FindPossibleAttackSpots(IntVector2 vectorD, int range)
    {
        CalculateHorizontalRange(range);
        CalculateHorizontalRange(range,-1);
        CalculateVerticalRange(range);
        CalculateVerticalRange(range, -1);
    }

    private void CalculateHorizontalRange(int range, int direction = 1)
    {
        for (int i = 1; i < range + 1; i++)
        {
            int realOffset = i * direction;
            bool linearVision = true;
            Debug.Log(selectedCharacter.myPos.x + realOffset);
            if ((selectedCharacter.myPos.x + realOffset) < MapCreator.mapSize && (selectedCharacter.myPos.x + realOffset) >= 0)
            {
                if (MapCreator.myMap[selectedCharacter.myPos.x + realOffset, selectedCharacter.myPos.y].passable && linearVision)
                {
                    MapCreator.myMap[selectedCharacter.myPos.x + realOffset, selectedCharacter.myPos.y].highlight(Color.cyan);
                }
                else
                {
                    MapCreator.myMap[selectedCharacter.myPos.x + realOffset, selectedCharacter.myPos.y].highlight(Color.cyan);
                    linearVision = false;
                }
            }      
        }
    }

    private void CalculateVerticalRange(int range, int direction = 1)
    {
        int sidewayRange = range;
       
        for (int i = 1; i < range + 1; i++)
        {
            int realOffset = i * direction;
            bool linearVision = true;
            if((selectedCharacter.myPos.y + realOffset) < MapCreator.mapSize && (selectedCharacter.myPos.y + realOffset) >= 0)
            {
                if (MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + realOffset].passable && linearVision)
                {
                    MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + realOffset].highlight(Color.cyan);
                }
                else
                {
                    MapCreator.myMap[selectedCharacter.myPos.x, selectedCharacter.myPos.y + realOffset].highlight(Color.cyan);
                    linearVision = false;
                }
                for (int j = 1; j < sidewayRange; j++)
                {
                    if ((selectedCharacter.myPos.x + j) < MapCreator.mapSize && (selectedCharacter.myPos.x + j) >= 0)
                    {
                        if (MapCreator.myMap[selectedCharacter.myPos.x + j, selectedCharacter.myPos.y + realOffset].passable)
                        {
                            MapCreator.myMap[selectedCharacter.myPos.x + j, selectedCharacter.myPos.y + realOffset].highlight(Color.cyan);
                        }
                        else
                        {
                            MapCreator.myMap[selectedCharacter.myPos.x + j, selectedCharacter.myPos.y + realOffset].highlight(Color.cyan);
                        }
                    }
                    if ((selectedCharacter.myPos.x - j) < MapCreator.mapSize && (selectedCharacter.myPos.x - j) >= 0)
                    {
                        if (MapCreator.myMap[selectedCharacter.myPos.x - j, selectedCharacter.myPos.y + realOffset].passable)
                        {
                            MapCreator.myMap[selectedCharacter.myPos.x - j, selectedCharacter.myPos.y + realOffset].highlight(Color.cyan);
                        }
                        else
                        {
                            MapCreator.myMap[selectedCharacter.myPos.x - j, selectedCharacter.myPos.y + realOffset].highlight(Color.cyan);
                        }
                    }                   
                }
            }
            sidewayRange--;
           
        }
        
    }
}
