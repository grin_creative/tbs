﻿using UnityEngine;

[System.Serializable]
public struct IntVector2
{
    public int x;
    public int y;

	public IntVector2 (int s, int b){
		x = s;
		y = b;
	}


 int sqrMagnitude
 {
     get { return x * x + y * y; }
 }

 public Vector2 toVector2(){
	 return new Vector2(x,y);
 } 

}
