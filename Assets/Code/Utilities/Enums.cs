﻿

public class Enums
{

    public enum skillType { SingleDamage, LineDamage, CircleDamage, ConeDamage, SquareDamage, CrossDamage, PlusDamage, Buff, Debuff };

    public enum buffEffects { Acc, Dmg, Range, Speed };

    public enum debuffEffects { Acc, Dmg, Range, Speed };
}
