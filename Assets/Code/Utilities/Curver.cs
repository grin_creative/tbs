﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Curver : MonoBehaviour
{
    //arrayToCurve is original Vector3 array, smoothness is the number of interpolations. 
    public static List<Vector3> MakeSmoothCurve(List<Vector3> arrayToCurve, float smoothness)
    {
        List<Vector3> points;
        List<Vector3> curvedPoints;
        int pointsLength = 0;
        int curvedLength = 0;

        if (smoothness < 1.0f) smoothness = 1.0f;

        pointsLength = arrayToCurve.Count;

        curvedLength = (pointsLength * Mathf.RoundToInt(smoothness)) - 1;
        curvedPoints = new List<Vector3>(curvedLength);

        float t = 0.0f;
        for (int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
        {
            t = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);

            points = new List<Vector3>(arrayToCurve);

            for (int j = pointsLength - 1; j > 0; j--)
            {
                for (int i = 0; i < j; i++)
                {
                    points[i] = (1 - t) * points[i] + t * points[i + 1];
                }
            }

            curvedPoints.Add(points[0]);
        }
        return curvedPoints;
    }

    public static Vector3 AdjustMenusByScreenBounds(int xRef, int yRef,float x, float y, Character c, RectTransform rT, float yInversed, bool b = false){
        Vector3 v = Vector3.zero;
        if(b == false){
        v = Camera.main.WorldToScreenPoint(c.transform.position);
        }
        else{
        v = rT.position*2;
        }
       
        float adjustmentX = (Camera.main.pixelRect.width - xRef) / xRef;
        float adjustmentY = (Camera.main.pixelRect.height - yRef) / yRef;
       
        if (v.x + rT.sizeDelta.x < Camera.main.pixelRect.width)
        {

            v.x += rT.sizeDelta.x * (x - (x * Mathf.Abs(adjustmentX)));
          
        }
        else
        {
            v.x -= rT.sizeDelta.x * (x - (x * Mathf.Abs(adjustmentX)));
           
        }        
        if (v.y + rT.sizeDelta.y < Camera.main.pixelRect.height)
        {
            v.y += rT.sizeDelta.y * ((y + yInversed) - (y * Mathf.Abs(adjustmentY)));            
        }
        else
        {
            v.y -= rT.sizeDelta.y * (y - (y * Mathf.Abs(adjustmentY)));
        }        
        return v;
        
    }

     public static Vector3 AdjustSubMenusByScreenBoundsAdditive(int xRef, int yRef,float x, float y, RectTransform waypoint, RectTransform rT, float yInversed){
        Vector3 v = waypoint.position;
        //rT.position = v;
        float adjustmentX = (Camera.main.pixelRect.width - xRef) / xRef;
        float adjustmentY = (Camera.main.pixelRect.height - yRef) / yRef;
       
        if (v.x + rT.sizeDelta.x < Camera.main.pixelRect.width)
        {
            Debug.Log(1);       

        }
        else
        {
            v.x -= 650; 
          Debug.Log(2);          
        }        
        Debug.Log(v.y + rT.sizeDelta.y);
        Debug.Log(Camera.main.pixelRect.height * 0.5f);
        if (v.y + rT.sizeDelta.y > Camera.main.pixelRect.height*0.5f)
        {
            Debug.Log(3);
           // v.y += rT.sizeDelta.y * ((y + yInversed) - (y * Mathf.Abs(adjustmentY)));            
        }
        else
        {
            Debug.Log(4);
            v.y += Camera.main.pixelRect.height*.25f;
        }  
        
        return v;        
    }
}