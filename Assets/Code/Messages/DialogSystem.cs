﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogSystem : MonoBehaviour {
    public bool canTalk = false;
    public bool scriptedTalkb = true;
    public Dialog currentDialog;
    public Transform NPCTalkArea;
    public AudioClip extraAudio;

    void Start() {
        NPCTalkArea = GameObject.Find("Canvas").transform.Find("NPCTalk");
        NPCTalkArea.gameObject.SetActive(false);
       // StartTalk();
    }
    void Update()
    {
        if(canTalk == true && scriptedTalkb == false)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                StartTalk();
            }
        }
    }

    public void StartTalk(){
                NPCTalkArea.gameObject.SetActive(true);
                NPCTalkArea.GetChild(1).GetComponent<Text>().text = currentDialog.name + " Says:";
                string currentMessage = currentDialog.Talk();
                
                if (currentMessage != "")
                {
                    NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
                }
                else
                {
                    CleanMessage();
                    if(currentDialog.specials == 1){                       
                        currentDialog.changePhase(2);
                                               
                    }
                }
    }

    public void StartScriptedTalk(){
        StartCoroutine(ScriptedTalk());
    }

    IEnumerator ScriptedTalk()
    {   
        scriptedTalkb = true;
      //  currentDialog.GetComponent<AudioSource>().Play();    
        yield return new WaitForSeconds(0.5f);
        string currentMessage = currentDialog.Talk();
        yield return new WaitForSeconds(0.5f);        
        NPCTalkArea.gameObject.SetActive(true);
        NPCTalkArea.GetChild(1).GetComponent<Text>().text = currentDialog.name + " Says:";
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(7.5f);
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(4f);
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(6.5f);
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(6f);
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(5f);
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(10.5f);        
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(10f);
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(15f);
       // currentDialog.GetComponent<AudioSource>().clip = extraAudio;
       // currentDialog.GetComponent<AudioSource>().Play();
        currentMessage = currentDialog.Talk();
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = currentMessage;
        yield return new WaitForSeconds(4.5f);               
        currentDialog.changePhase(2);
        scriptedTalkb = false;
        canTalk = false;
        currentDialog = null;
        CleanMessage();
    }

    void CleanMessage()
    {
        NPCTalkArea.gameObject.SetActive(false);
        NPCTalkArea.GetChild(1).GetComponent<Text>().text = "";
        NPCTalkArea.GetChild(3).GetComponent<Text>().text = "";          
    }


    
}
