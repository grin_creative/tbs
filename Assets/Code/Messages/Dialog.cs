﻿ using UnityEngine;
using System.Collections;
using System;
using UnityEngine.AI;

public class Dialog : MonoBehaviour {

    // Use this for initialization
    protected bool isTalkable = false;
    [SerializeField]
    protected string[] totalDialogs;
    protected int[] currentDialogs;

    public int[] talkingSets;
    protected int selectDialog; 
    int lastDialog = 0;   

    [SerializeField]
    protected int phases;
    public int specials = 0;

    NavMeshAgent myAgent;

    Animator myAnimator;

    int talkAnim = 0;

    

    protected virtual void Start()
    {
        changePhase(0);       
        isTalkable = true;
     
    }

    
    public virtual string Talk()
    {     
            if (selectDialog == currentDialogs.Length)
            {
                selectDialog = 0;
                return "";
            }
            else if(isTalkable == false){
                selectDialog = 0;
                return "";
            }           
            else
            {
                selectDialog++;
            }        
            return totalDialogs[currentDialogs[selectDialog-1]];
             
    }   
    public virtual void changePhase(int i)
    {
        
            isTalkable = false;
            currentDialogs = new int[talkingSets[i]];
            int count = lastDialog;
            for( int f = 0; f < talkingSets[i]; f++){
                currentDialogs[f] = count;
                count++;
            }
            lastDialog = count;
            selectDialog = 0;           
        
    }

}
