﻿using UnityEngine.UI;
using UnityEngine;

public class AdjustGrit : MonoBehaviour {

	public void Adjust()
    {
        var grid = GetComponent<GridLayoutGroup>();
        var gridTransform = GetComponent<RectTransform>();
        Vector2 newPos = gridTransform.anchoredPosition;
        Vector2 oldSize = gridTransform.sizeDelta;
        Vector2 newSize = gridTransform.sizeDelta;
        int size = gridTransform.childCount -1;    
        gridTransform.sizeDelta = newSize;
        gridTransform.anchoredPosition = newPos;
        gridTransform.offsetMin = new Vector2(gridTransform.offsetMin.x, -0.5f * (grid.padding.top + grid.padding.bottom + (grid.cellSize.y * size) + (grid.spacing.y * size)));
        gridTransform.offsetMax = new Vector2(gridTransform.offsetMax.x, 0);
    }
}
