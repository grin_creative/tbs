# Turn Based System #

This started as a hobby, I kinda like this kind of game, to more of a template that I used to a professional project then expanded this from time to time when I have idle time

### Components ###

* Pathfinding - 95%, fully functional and fast but I'm not giving as finished.
* Wireframe for Tiles 3D - 95%, same as above.
* Basic AI - 100%.
* Advanced AI - 0%.
* Dialogue / Speech System - 70%, copied from another project made by me needs basic settings and some UIs to be implemented.
* Turn Management System - 100%.
* System Skills / Skills - 60%, lack the application with both types of AI, the player part is already complete.
* Modular Combat System - 90%, missing final adjustments and some decisions ex: Basic Attack be a Skill so there is no separation.
* Audio-by-Event System - 40% - grabbed from my other project, missing implementation and modifications.

### AI ###

As it's complicated to explain it in the components section, I'm expanding the topic here, basically it
make up the thinking and acting of enemies during combat only.
This composes movement, target acquisition logic, attack method and positioning in the field, being divided into 2 AI methods.

The basic AI is the logic of the aggressive enemy, that is, the enemy that acquire an target with a random method and aims to attack him until one of the 2 die, then repeating the cicle.

AI Advanced is the most complex archetypes, such as defensive( I don't mean a wall here), flanking, support and skirmisher/kiter.

### Project's goal ###

Initially it was only to be done in the free time between my works or when I has a hobby, but how it is progressing fast I see the possibility of trying to
get something out of it by selling it to third-party projects or even the Unity Store.
A distant option is to convert this to blueprints (I think C++ is only necessary for pathfinding, the rest is banal) in UE4 and make a cool demo, and sell in EU Store as well.